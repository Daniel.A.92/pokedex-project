package com.example.pokedex

import android.content.res.Resources
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.focus.FocusState
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import com.example.pokedex.model.Pokemon
import java.io.BufferedReader
import java.io.InputStreamReader

fun readPokemon(resources: Resources?): List<Pokemon> {
    val br = BufferedReader(InputStreamReader(resources?.openRawResource(R.raw.pokemondata)))
    val pokemonList = mutableListOf<Pokemon>()
    br.readLine()
    var line = br.readLine()

    while (line != null) {
        val splLine = line.split(",")
        val poke = Pokemon(
            nr = splLine[0].toInt(),
            name = splLine[1],
            type1 = splLine[2],
            type2 = splLine[3],
            total = splLine[4].toInt(),
            hp = splLine[5].toInt(),
            attack = splLine[6].toInt(),
            defense = splLine[7].toInt(),
            spAtk = splLine[8].toInt(),
            spDef = splLine[9].toInt(),
            speed = splLine[10].toInt(),
            generation = splLine[11].toInt(),
            legendary = splLine[12].toBoolean(),
            imagePath = null
        )
        pokemonList += poke
        line = br.readLine()
    }
    return pokemonList

}

@Composable
fun SearchBar(
    modifier: Modifier = Modifier,
    hint: String = "Search...",
    onSearch: (String) -> Unit = {}
) {

    var text by remember {
        mutableStateOf("")
    }

    var isHintDisplayed by remember {
        mutableStateOf(hint != "")
    }

    Box(modifier = modifier) {
        BasicTextField(
            value = text,
            onValueChange = {
                text = it
                onSearch(it)
            },
            maxLines = 1,
            singleLine = true,
            textStyle = TextStyle(color = Color.Black),
            modifier = Modifier
                .fillMaxWidth()
                .shadow(5.dp, CircleShape)
                .background(Color.White, CircleShape)
                .padding(horizontal = 20.dp, vertical = 12.dp)
                .onFocusChanged {
                  // isHintDisplayed = it != FocusState
                }
        )
        if (isHintDisplayed) {
            Text(
                text = hint,
                color = Color.LightGray,
                modifier = Modifier
                    .padding(horizontal = 20.dp, vertical = 12.dp)
            )
        }
    }
}