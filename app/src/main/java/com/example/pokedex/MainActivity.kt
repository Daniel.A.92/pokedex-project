package com.example.pokedex

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.pokedex.landingPage.LandingPage
import com.example.pokedex.viewModel.PokemonDetailScreen
import com.example.pokedex.view.PokemonListScreen


class MainActivity : ComponentActivity() {
    @SuppressLint("RememberReturnType")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            val pokeList = readPokemon(resources = resources)
            val navController = rememberNavController()

            NavHost(
                navController = navController,
                startDestination = "landing_page"
            ) {
                composable("landing_page") {
                    LandingPage() {
                        navController.navigate("pokemon_list_screen")
                    }
                }

               composable("pokemon_list_screen") {
                    PokemonListScreen(navController = navController, pokemonList = pokeList,
                        onNavigateToPokemonDetail = { navController.navigate("pokemon_detail_screen/$it") })
                }
                composable("pokemon_detail_screen/{pokemonName}",
                    arguments = listOf(
                        navArgument("pokemonName") {
                            type = NavType.StringType
                        }
                    )

                ) {
                    val pokemonName = remember {
                        it.arguments?.getString("pokemonName")
                    }
                    val currentPoke = pokeList.find { x -> x.name.equals(pokemonName) }!!
                    PokemonDetailScreen(pokemon = currentPoke)
                }
            }
        }
    }
}


