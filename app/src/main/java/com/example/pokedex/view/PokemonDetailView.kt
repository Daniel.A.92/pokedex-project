package com.example.pokedex.viewModel


import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.pokedex.model.Pokemon


@Composable
fun PokemonDetailScreen(
    pokemon: Pokemon,
) {
    Column(
        Modifier.background(Color.Cyan),
        verticalArrangement = Arrangement.SpaceEvenly,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        val context = LocalContext.current

        Text("Gen: ${pokemon.generation}", fontWeight = FontWeight.Bold, fontSize = 35.sp)

        Image(
            modifier = Modifier
                .size(450.dp)
                .background(color = Color.Cyan),

            painter = painterResource(
                id = context.resources.getIdentifier(
                    pokemon.name.lowercase(), "drawable", context.packageName
                )
            ),
            contentDescription = pokemon.name,
        )

        Column(
            modifier = Modifier
                //    .padding(top = 15.dp)
                .fillMaxSize()
                .background(Color.Cyan),
            verticalArrangement = Arrangement.SpaceEvenly,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {

            Text("#${pokemon.nr} ${pokemon.name}", fontWeight = FontWeight.Bold, fontSize = 35.sp)
            Text(text = "Type: ${pokemon.type1} / ${pokemon.type2}", fontSize = 30.sp, fontFamily = FontFamily.Serif)
            Text(text = "HP:  ${pokemon.hp}", fontSize = 30.sp, fontFamily = FontFamily.Serif)
            Text(text = "Defence: ${pokemon.defense}", fontSize = 30.sp, fontFamily = FontFamily.Serif)
            Text(text = "Attack: ${pokemon.attack}", fontSize = 30.sp, fontFamily = FontFamily.Serif)
        }
    }
}


