package com.example.pokedex.view

import android.media.MediaPlayer
import androidx.compose.runtime.remember
import androidx.compose.runtime.*
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.example.pokedex.R
import com.example.pokedex.SearchBar
import com.example.pokedex.model.Pokemon
import androidx.compose.runtime.remember as remember


@Composable
fun PokemonListScreen(
    pokemonList: List<Pokemon>,
    onNavigateToPokemonDetail: (String) -> Unit,
    navController: NavHostController
) {
        val context = LocalContext.current


    Column(modifier = Modifier.background(Color.Cyan)) {


            Spacer(modifier = Modifier.height(20.dp))
            Image(
                painter = painterResource(id = R.drawable.ic_international_pok_mon_logo),
                contentDescription = "Pokemon",
                modifier = Modifier
                    .fillMaxWidth()
                    .align(CenterHorizontally)
            )

            SearchBar(
                hint = "Search...",
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp)
            )

            LazyVerticalGrid(columns = GridCells.Adaptive(75.dp), content = {
                items(pokemonList) { x ->

                    Column() {
                        Text(x.name, fontWeight = FontWeight.Bold)
                        val id = context.resources.getIdentifier(
                            x.name.lowercase(), "drawable", context.packageName
                        )
                        println(id)
                        if (id != 0) {
                            Image(
                                modifier = Modifier
                                    .size(100.dp)
                                    .clickable { onNavigateToPokemonDetail(x.name) },
                                painter = painterResource(id = id),
                                contentDescription = x.name,
                            )
                        }
                    }
                }
            })
        }
    }












