package com.example.pokedex.model
//#,Name,Type 1,Type 2,Total,HP,Attack,Defense,Sp. Atk,Sp. Def,Speed,Generation,Legendary
data class Pokemon(
    val nr:Int,
    val name:String,
    val type1:String,
    val type2:String?,
    val total:Int,
    val hp:Int,
    val attack:Int,
    val defense:Int,
    val spAtk:Int,
    val spDef:Int,
    val speed:Int,
    val generation:Int,
    val legendary:Boolean,
    val imagePath:Int?) {
}